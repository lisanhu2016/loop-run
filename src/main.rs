fn main() {
    let cfg = parse();
    std::process::exit(run(&cfg));
}

struct Config {
    program: String,
    delay: u64,
}

fn parse() -> Config {
    use clap::{App, Arg};
    let matches = App::new("Run command continuously with certain delay between calls")
        .arg(Arg::with_name("program").required(false).default_value("nvidia-smi"))
        .arg(Arg::with_name("delay").required(false).default_value("100"))
        .get_matches();
    let program = matches.value_of("program").unwrap().to_owned();
    let delay = matches.value_of("delay").unwrap();
    let delay = delay.parse::<u64>().expect("Failed to parse delay");
    Config { program, delay }
}

fn run(cfg: &Config) -> i32 {
    use crossterm::execute;
    use crossterm::terminal::*;
    use std::io::{stdout, Write};

    // enable_raw_mode().expect("Failed to enable raw mode");
    loop {
        let mut stdout = stdout();
        let mut command = std::process::Command::new(&cfg.program);
        let output = command
            .output()
            .expect(&format!("Failed to execute command {}", cfg.program));
        execute!(stdout, Clear(ClearType::All));
        stdout
            .write_all(&output.stdout)
            .expect("Error writing to the stdout buffer");

    enable_raw_mode().expect("Failed to enable raw mode");
        if let Ok(true) = check_quit(cfg.delay) {
            break;
        }
    disable_raw_mode().expect("Failed to disable raw mode");
    }
    disable_raw_mode().expect("Failed to disable raw mode");
    0
}

fn check_quit(delay: u64) -> Result<bool, Box<dyn std::error::Error>> {
    use crossterm::event::*;
    use std::time::Duration;

    if poll(Duration::from_millis(delay))? {
        if read()? == Event::Key(KeyEvent::from(KeyCode::Char('q'))) {
            return Ok(true);
        } else {
            return Ok(false);
        }
    }
    Ok(false)
}
